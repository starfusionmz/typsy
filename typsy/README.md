#Typsy
A repository for sharing semantic meanings of disparate datasets among disparate groups
The goal here is that if the dataset producer uses this api to store their datasets, Consumers can have tools to discover semantic meaning from the data after the fact

example:

```java
   LinkFilterBuilder filterBuilder = service.createLabelFilter();
    CellFilter tomatofilter = filterBuilder.on(LinkDirection.EITHER)
        .where(
            filterBuilder.label().eq(tomato_fruit).and(filterBuilder.linktype().eq(LinkType.ISA)))
        .build();
    List<Cell> tomatoResults = reader.stream().filter(tomatofilter).collect(Collectors.toList());
```

##Cell
 (Label,Visibility,Value)

##Type 
  types are static and will only be added on library updates currently the following are:


###Boolean

###Byte Array

###Long
 a signed long

###Double
 a double 

###String
 a utf8 value

###OrderedList
  a order collection of something. All types have to be the same type.

###Map
  key value pair where key reference something of all the same type

###Object: this will be used for schema
  similar to map but keys and value types are predefined 
  value types can be any of the valid types

##Namespace
A grouping of labels. A namespace is maintained by a set of owners.

##Label
Something that has semantic meaning. There is no global definition of label they only exist in a namespace. To link meaning use links

##Link
 a relationship between Labels
 a link has a source and destination to Label
 Links have different types and can also have a description
###Link Types:
####Is A
The two labels have a is a relationshop
####Has A 
The source label contains dest label
####Supercedes
The source label takes the place of the dest label
 ...more to come
`Open Question`: Currently linktype is not crowd sourced does it make sense to do so?
 