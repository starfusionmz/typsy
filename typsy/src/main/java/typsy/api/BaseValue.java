package typsy.api;

import typsy.api.types.Type;

/**
 * naive approach to holding values
 * 
 * @author Mark Lester
 *
 */
public class BaseValue implements Value {
  private Object value;
  private Type type;

  public BaseValue(Type type, Object value) {
    this.type = type;
    this.value = value;
  }

  @Override
  public <T> T getValueAs(Class<T> type) {
    return this.type.getValueAs(type,value);
  }

  @Override
  public Type getType() {
    return type;
  }

}
