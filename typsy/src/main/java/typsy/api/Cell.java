package typsy.api;

import typsy.api.label.Label;
import typsy.api.visibility.Visibility;

/**
 * a piece of information that can be stored somewhere
 * 
 * @author Mark Lester
 *
 */
public interface Cell {
  /**
   * initial semantic meaning given to cell by data producers. Follow on meanings will be stored in
   * the Typsy Web Service
   * 
   * @return
   */
  Label getLabel();
  
  /**
   * Visibility of the piece of information
   * @return
   */
  Visibility getVisibility();
  
  /**
   * the actual data
   * @return
   */
  Value getValue();
}
