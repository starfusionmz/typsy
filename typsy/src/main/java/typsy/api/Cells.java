package typsy.api;

import typsy.api.label.Label;
import typsy.api.visibility.Visibility;
/**
 * Common Utils for creating {@link Cell}s
 */
public class Cells {

	public static Cell create(Label label, Visibility vis, Value value) {
		return new DefaultCell(label, vis, value);
	}
	
}