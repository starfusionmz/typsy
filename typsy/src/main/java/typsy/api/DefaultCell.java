package typsy.api;

import typsy.api.label.Label;
import typsy.api.visibility.Visibility;

/**
 * naive first run implementation of a {@link Cell}
 * 
 * @author Mark Lester
 *
 */
public class DefaultCell implements Cell {
  private final Label label;
  private final Visibility vis;
  private final Value value;

  public DefaultCell(Label label, Visibility vis, Value value) {
    this.vis = vis;
    this.value = value;
    this.label = label;
  }

  @Override
  public Value getValue() {
    return value;
  }

  @Override
  public Visibility getVisibility() {
    return vis;
  }

  @Override
  public Label getLabel() {
    return label;
  }
  
  @Override
  public String toString() {
    return "DefaultCell [label=" + label + ", vis=" + vis + ", value=" + value + "]";
  }
}
