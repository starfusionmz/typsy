package typsy.api;

import javax.sql.DataSource;

public class TypsyRegistry {
  
  private TypsyRegistry() {}

  public static TypsyService getService(DataSource ds) {
//    HikariDataSource datasource = new HikariDataSource();
//    URL url = TypsyRegistry.class.getResource("typsy.db");
//    datasource.setJdbcUrl(url.getPath());
//    datasource.setUsername("sa");
//    datasource.setPassword("sa");
    return new TypsyService(ds);
  }
}
