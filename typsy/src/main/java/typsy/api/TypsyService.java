package typsy.api;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import typsy.api.filter.fluent.LinkFilterBuilder;
import typsy.service.BasicNamespaceService;
import typsy.service.LabelService;
import typsy.service.LinkService;
import typsy.service.NamespaceService;

public class TypsyService {
  private final NamespaceService namespaceService;
  private final LinkService linkService;
  private final LabelService labelService;
  private DSLContext ctx;
  
  public TypsyService(DataSource datasource) {
    ctx = DSL.using(datasource,SQLDialect.H2);
    this.namespaceService = new BasicNamespaceService(ctx);
    this.labelService = new LabelService(ctx, namespaceService);
    this.linkService = new LinkService(ctx,labelService);

  }

  public LinkService getLinkService() {
    return linkService;
  }

  public LabelService getLabelService() {
    return labelService;
  }

  public NamespaceService getNamespaceService() {
    return namespaceService;
  }
  
  public LinkFilterBuilder createLabelFilter(){
    return new LinkFilterBuilder(this);
  }
  
//  TypeFilterBuilder createTypeFilter(){
//    return null; 
//  }

  public DSLContext getContext() {
    return ctx;
  }
}
