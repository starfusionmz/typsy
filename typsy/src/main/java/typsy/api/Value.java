package typsy.api;

import typsy.api.types.Type;

/**
 * used to contain a value for a {@link Cell}
 * 
 * @author Mark Lester
 *
 */
public interface Value {
  /**
   * determines how this data will be retrieved and stored
   * 
   * @return
   */
  Type getType();
  
  /**
   * using the type try to coerce the value to the given Java Type
   * @param type
   * @return
   */
  <T> T getValueAs(Class<T> type);
}
