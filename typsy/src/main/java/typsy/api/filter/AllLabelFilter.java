package typsy.api.filter;

import java.util.Set;

import typsy.api.Cell;

/**
 * filter to check if the given cell matches all of {@link LabelMatcher} in a set
 * 
 * @author Mark Lester
 *
 */
public class AllLabelFilter implements CellFilter {
  private Set<LabelMatcher> labelMatchers;

  public AllLabelFilter(Set<LabelMatcher> matchLabels) {
    this.labelMatchers = matchLabels;
  }

  @Override
  public boolean apply(Cell input) {
    for (LabelMatcher labelMatcher : labelMatchers) {
      if (!labelMatcher.apply(input.getLabel())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean test(Cell cell) {
    return apply(cell);
  }
}
