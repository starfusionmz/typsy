package typsy.api.filter;

import java.util.Set;

import typsy.api.Cell;

/**
 * filter to check if the given cell matches any one of the set of {@link LabelMatcher}
 * 
 * @author Mark Lester
 *
 */
public class AnyLabelFilter implements CellFilter {
  private Set<LabelMatcher> labelMatchers;

  public AnyLabelFilter(Set<LabelMatcher> matchLabels) {
    this.labelMatchers = matchLabels;
  }

  @Override
  public boolean apply(Cell input) {
    for (LabelMatcher labelMatcher : labelMatchers) {
      if (labelMatcher.apply(input.getLabel())) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean test(Cell cell) {
    return apply(cell);
  }
}
