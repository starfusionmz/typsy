package typsy.api.filter;

import com.google.common.base.Predicate;

import typsy.api.Cell;

/**
 * interface to apply a filter on a {@link Cell} using Google Stream API incase we want to use those
 * libs
 * 
 * @author Mark Lester
 *
 */
public interface CellFilter extends Predicate<Cell>, java.util.function.Predicate<Cell> {
}
