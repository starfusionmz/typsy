package typsy.api.filter;

import com.google.common.base.Predicate;

import typsy.api.label.Label;

/**
 * matches a label based on {@link MatchMode}
 * 
 * @author Mark Lester
 *
 */
public class LabelMatcher implements Predicate<Label> {
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((label == null) ? 0 : label.hashCode());
    result = prime * result + ((mode == null) ? 0 : mode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    LabelMatcher other = (LabelMatcher) obj;
    if (label == null) {
      if (other.label != null)
        return false;
    } else if (!label.toString().equals(other.label.toString()))
      return false;
    if (mode != other.mode)
      return false;
    return true;
  }

  public enum MatchMode {
    /**
     * match on both pieces of the {@link Label}
     */
    BOTH,
    /**
     * match only on the name
     */
    LABEL,
    /**
     * match on on the namespace
     */
    NAMESPACE;
  }

  MatchMode mode = MatchMode.BOTH;
  private final Label label;

  public LabelMatcher(Label label, MatchMode mode) {
    this.mode = mode;
    this.label = label;
  }

  @Override
  public boolean apply(Label input) {
    switch (mode) {
      case BOTH:
        return label.getName().equals(input.getName())&&label.getNamespace().equals(input.getNamespace());
      case LABEL:
        return label.getName().equals(input.getName());
      case NAMESPACE:
        return label.getNamespace().equals(input.getNamespace());
    }
    return false;
  }
}
