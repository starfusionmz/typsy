package typsy.api.filter.fluent;

import static typsy.db.jooq.generated.tables.Links.LINKS;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectOnConditionStep;

import typsy.db.jooq.generated.tables.Labels;
import typsy.db.jooq.generated.tables.Namespaces;

public interface LinkDirection {
  public static final LinkDirection SOURCE = new LinkDirection() {

    @Override
    public SelectOnConditionStep<Record> configure(DSLContext ctx) {
      return ctx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
          .join(Namespaces.NAMESPACES).on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID));
    }

  };
  public static final LinkDirection DESTINATION = new LinkDirection() {

    @Override
    public SelectOnConditionStep<Record> configure(DSLContext ctx) {
      return ctx.select().from(LINKS).join(Labels.LABELS)
          .on(LINKS.DESTINATION.eq(Labels.LABELS.ID)).join(Namespaces.NAMESPACES)
          .on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID));
    }

  };
  public static final LinkDirection EITHER = new LinkDirection() {

    @Override
    public SelectOnConditionStep<Record> configure(DSLContext ctx) {
      return ctx.select().from(LINKS).join(Labels.LABELS)
          .on(LINKS.DESTINATION.eq(Labels.LABELS.ID).or(LINKS.SOURCE.eq(Labels.LABELS.ID)))
          .join(Namespaces.NAMESPACES).on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID));
    }

  };

  /**
   * needed to configured the direction portion of the query
   * 
   * @param ctx
   * @return
   */
  SelectOnConditionStep<Record> configure(DSLContext ctx);
}