package typsy.api.filter.fluent;

import static typsy.db.jooq.generated.tables.Links.LINKS;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.Condition;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.SelectOnConditionStep;

import com.google.common.collect.Lists;
import com.google.common.collect.Table.Cell;

import typsy.api.TypsyService;
import typsy.api.filter.AllLabelFilter;
import typsy.api.filter.AnyLabelFilter;
import typsy.api.filter.CellFilter;
import typsy.api.filter.LabelMatcher;
import typsy.api.filter.LabelMatcher.MatchMode;
import typsy.api.filter.fluent.fields.FieldBuilder;
import typsy.api.filter.fluent.fields.LabelFieldBuilder;
import typsy.api.filter.fluent.fields.LinkTypeFieldBuilder;
import typsy.api.link.BasicLink;
import typsy.api.link.Link;
import typsy.api.link.LinkType;
import typsy.web.model.LabelModel;

/**
 * Allows for a a user to build a filter that can be used to filter {@link Cell}s by {@link Link}s
 * Ex: LinkFilterBuilder filterBuilder = service.createLabelFilter(); CellFilter labelFilter =
 * filterBuilder.on(LinkDirection.SOURCE)
 * .where(filterBuilder.linktype().eq(LinkType.ISA).and(filterBuilder.label().eq(myip_label)))
 * .build();
 * 
 * @author Mark Lester
 *
 */
public class LinkFilterBuilder {
  /**
   * How the filter should match on the cells
   * 
   * @author Mark Lester
   *
   */
  enum CellMatchType {
    /**
     * match on any of the critera gathers from the this filter
     */
    ANY,
    /**
     * must match on all criteria
     */
    ALL
  }

  private TypsyService service;

  private CellMatchType cellMatchType = CellMatchType.ANY;

  private LinkDirection linkDirection;

  private List<Condition> conditions = Lists.newArrayList();

  public LinkFilterBuilder(TypsyService service) {
    this.service = service;
  }

  public LinkFilterBuilder on(LinkDirection direction) {
    this.linkDirection = direction;
    return this;
  }

  public LinkFilterBuilder where(FieldBuilder field) {
    conditions.add(field.build());
    return this;
  }



  public LinkFilterBuilder match(CellMatchType matchType) {
    this.cellMatchType = matchType;
    return this;
  }

  public CellFilter build() {
    SelectOnConditionStep<Record> query = linkDirection.configure(service.getContext());
    List<Link> links = query.where(conditions).fetch(new RecordMapper<Record, Link>() {

      @Override
      public Link map(Record record) {
        int dstId = record.getValue(LINKS.DESTINATION);
        int srcId = record.getValue(LINKS.SOURCE);
        LinkType linkType = LinkType.valueOf(record.getValue(LINKS.LINKTYPE));
        LabelModel dest = service.getLabelService().findById(dstId);
        LabelModel src = service.getLabelService().findById(srcId);
        return new BasicLink(src, dest, linkType);
      }
    });

    Set<LabelMatcher> labelMatchers = links.stream().map(link -> {
      if (link.getLinkType().equals(LinkType.ISA) || linkDirection == LinkDirection.EITHER) {
        return Lists.newArrayList(new LabelMatcher(link.getDestination(), MatchMode.BOTH),
            new LabelMatcher(link.getSource(), MatchMode.BOTH));
      }
      if (linkDirection == LinkDirection.SOURCE) {
        return Collections.singletonList(new LabelMatcher(link.getDestination(), MatchMode.BOTH));
      }
      return Collections.singletonList(new LabelMatcher(link.getSource(), MatchMode.BOTH));
    }).flatMap(lmatcher -> lmatcher.stream()).collect(Collectors.toSet());


    if (cellMatchType.equals(CellMatchType.ANY)) {
      return new AnyLabelFilter(labelMatchers);
    } else {
      return new AllLabelFilter(labelMatchers);
    }
  }

  public LabelFieldBuilder label() {
    return new LabelFieldBuilder(service.getContext());
  }

  public LinkTypeFieldBuilder linktype() {
    return new LinkTypeFieldBuilder();
  }

}
