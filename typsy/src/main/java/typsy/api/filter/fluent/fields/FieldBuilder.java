package typsy.api.filter.fluent.fields;

import org.jooq.Condition;


/**
 * common methods to allow join FieldBuilder Conditions
 * 
 * @author Mark Lester
 *
 */
public interface FieldBuilder {
  public FieldBuilder or(FieldBuilder builder);

  public FieldBuilder and(FieldBuilder builder);

  public Condition build();

}
