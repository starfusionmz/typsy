package typsy.api.filter.fluent.fields;

import static typsy.db.jooq.generated.tables.Labels.LABELS;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.jooq.Condition;
import org.jooq.DSLContext;

import typsy.api.label.Label;
import typsy.db.jooq.generated.tables.Labels;
import typsy.db.jooq.generated.tables.Namespaces;

/**
 * Field build for matching on label
 * @author Mark Lester
 *
 */
public class LabelFieldBuilder implements FieldBuilder {
  private Condition condition;
  private DSLContext ctx;
  
  public LabelFieldBuilder(DSLContext ctx) {
    this.ctx = ctx;
  }
  

  @Override
  public FieldBuilder and(FieldBuilder builder) {
    condition = condition.and(builder.build());
    return this;
  }

  @Override
  public FieldBuilder or(FieldBuilder builder) {
    condition = condition.or(builder.build());
    return this;
  }

  @Override
  public Condition build() {
    return condition;
  }

  public FieldBuilder eq(Label label) {
    Integer labelId = ctx.select().from(LABELS).join(Namespaces.NAMESPACES)
    .on(LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID)).where(LABELS.NAME.eq(label.getName())
        .and(Namespaces.NAMESPACES.NAME.eq(label.getNamespace()))).fetchAny(LABELS.ID);
    condition = Labels.LABELS.ID.eq(labelId);
    return this;
  }

  public FieldBuilder in(Collection<Label> labels) {
    List<Integer> labelIds = Collections.emptyList();
    Optional<Condition> labelIdCondition =
        labels.stream().map(label -> LABELS.NAME.eq(label.getName())
            .and(Namespaces.NAMESPACES.NAME.eq(label.getNamespace()))).reduce((c1, c2) -> {
              c1.or(c2);
              return c2;
            });
    if(labelIdCondition.isPresent()){
      labelIds = ctx.select().from(LABELS).join(Namespaces.NAMESPACES)
          .on(LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID)).where(labelIdCondition.get()).fetch(LABELS.ID);
    }
    condition = Labels.LABELS.ID.in(labelIds);
    return this;
  }
}