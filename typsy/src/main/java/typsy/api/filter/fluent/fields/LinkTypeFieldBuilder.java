package typsy.api.filter.fluent.fields;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.jooq.Condition;

import typsy.api.link.LinkType;
import typsy.db.jooq.generated.tables.Links;

public class LinkTypeFieldBuilder implements FieldBuilder {
	private Condition condition;

	@Override
	public FieldBuilder and(FieldBuilder builder) {
		condition = condition.and(builder.build());
		return this;
	}

	@Override
	public FieldBuilder or(FieldBuilder builder) {
		condition = condition.or(builder.build());
		return this;
	}

	@Override
	public Condition build() {
		return condition;
	}

	public FieldBuilder eq(LinkType value) {
		condition = Links.LINKS.LINKTYPE.eq(value.toString());
		return this;
	}

	public FieldBuilder in(Collection<LinkType> linktypes) {
		List<String> values = linktypes.stream().map(input -> input.toString()).collect(Collectors.toList());
		condition = Links.LINKS.LINKTYPE.in(values.toString());
		return this;
	}
}