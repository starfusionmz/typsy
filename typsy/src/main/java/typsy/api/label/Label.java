package typsy.api.label;

/**
 * contains information about the label serialized to disk. This might be switch to and id? Or 2
 * interface once with everything and one with the id
 * 
 * @author Mark Lester
 *
 */
public interface Label {
  /**
   * name of the label
   * 
   * @return
   */
  String getName();

  /**
   * description of the label
   *
   * @return
   */
  String getDescription();

  /**
   * the namespace the label resides in
   * 
   * @return
   */
  String getNamespace();
}
