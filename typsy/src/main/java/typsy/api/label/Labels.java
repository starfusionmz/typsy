package typsy.api.label;

/**
 * utiltiies and factories for {@link Label}
 * 
 * @author Mark Lester
 *
 */
public class Labels {
  private Labels() {}

  public static Label create(String label, String namespace, String description) {
    return new SimpleLabel(label, namespace, description);
  }
  
  public static Label create(String label, String namespace) {
    return new SimpleLabel(label, namespace, "");
  }
}
