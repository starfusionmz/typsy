package typsy.api.label;

public class SimpleLabel implements Label {
  private final String name;
  private final String namespace;
  private final String description;

  public SimpleLabel(String name, String namespace, String description) {
    this.name = name;
    this.namespace = namespace;
    this.description = description;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String getNamespace() {
    return namespace;
  }

  @Override
  public String toString() {
    return getName() + "@" + getNamespace() + " description: [" + getDescription() + "]";
  }
}
