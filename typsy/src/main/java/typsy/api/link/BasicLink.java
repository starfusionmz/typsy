package typsy.api.link;

import typsy.api.label.Label;

public class BasicLink implements Link{
  @Override
  public String toString() {
    return "BasicLink [source=" + source + "\n destination=" + destination + "\n linkType=" + linkType
        + "]";
  }

  private final Label source;
  private final Label destination;
  private final LinkType linkType;
  
  public BasicLink(Label source, Label destination,LinkType linkType) {
    this.source = source;
    this.destination = destination;
    this.linkType = linkType;
  }

  @Override
  public LinkType getLinkType() {
    return linkType;
  }

  @Override
  public Label getSource() {
    return source;
  }

  @Override
  public Label getDestination() {
    return destination;
  }
  
}
