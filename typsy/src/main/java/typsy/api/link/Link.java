package typsy.api.link;

import typsy.api.label.Label;

public interface Link {
	LinkType getLinkType();
	Label getSource();
	Label getDestination();
}
