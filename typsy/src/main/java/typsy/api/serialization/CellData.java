package typsy.api.serialization;

import typsy.api.Cell;

/**
 * used to store information about {@link Cell}
 * 
 * @author Mark Lester
 *
 */
class CellData{
  public static CellData fromCell(Cell cell) {
    CellData data = new CellData();
    data.setVis(cell.getVisibility().toString());
    data.setLabel(cell.getLabel().getName());
    data.setNamespace(cell.getLabel().getNamespace());
    data.setType(cell.getValue().getType().getName());
    data.setValue(cell.getValue().getValueAs(String.class));
    return data;
  }

  String vis;
  String label;
  String namespace;
  String type;
  String value;

  public String getVis() {
    return vis;
  }

  public void setVis(String vis) {
    this.vis = vis;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

}
