package typsy.api.serialization;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;

import typsy.api.Cell;
import typsy.api.Cells;
import typsy.api.Value;
import typsy.api.label.Label;
import typsy.api.label.Labels;
import typsy.api.types.Type;
import typsy.api.types.Types;
import typsy.api.visibility.Visibilities;
import typsy.api.visibility.Visibility;

/**
 * Serialize cells to json
 * are separated by spaces
 * 
 * @author Mark Lester
 *
 */
public class JsonStrategy implements SerializationStrategy {

	@Override
	public Writer createWriter(OutputStream outStream) throws IOException {
		return new JsonWriter(outStream);
	}

	@Override
	public Reader createReader(InputStream inStream) throws IOException {
		return new JsonReader(inStream);
	}

}

class JsonWriter implements Writer {
	private JsonGenerator generator;

	public JsonWriter(OutputStream outStream) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonFactory f = mapper.getFactory();
		generator = f.createGenerator(outStream);
		generator.writeStartArray();
	}

	@Override
	public void write(Cell cell) throws IOException {
		generator.writeObject(CellData.fromCell(cell));
	}

	@Override
	public void close() throws IOException {
	    generator.writeEndArray();
		generator.close();
	}
}

class JsonReader implements Reader {
	private Iterable<CellData> iterable;

	public JsonReader(InputStream is) throws JsonParseException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		MappingIterator<CellData> iterator = mapper.readerFor(CellData.class).readValues(is);
		iterable = () -> iterator;
	}

	@Override
	public Iterator<Cell> iterator() {	  
		return StreamSupport.stream(iterable.spliterator(), false).map((CellData cellData) -> {
			// description isn't needed for the cell.
			Label label = Labels.create(cellData.getLabel(), cellData.getNamespace(), null);
			Optional<Type> type = Types.find(cellData.getType());
			Value value = type.get().parse(cellData.getValue());
			Visibility vis = Visibilities.create(cellData.getVis());
			return Cells.create(label, vis, value);
		}).iterator();
	}

}
