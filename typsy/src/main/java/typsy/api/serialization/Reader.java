package typsy.api.serialization;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import typsy.api.Cell;

/**
 * Interface to read {@link Cell}s from a source
 */
public interface Reader extends Iterable<Cell> {
  default Stream<Cell> stream() {
    return StreamSupport.stream(spliterator(), false);
  }
}
