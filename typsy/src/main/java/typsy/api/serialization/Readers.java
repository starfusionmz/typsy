package typsy.api.serialization;

import java.io.IOException;
import java.io.InputStream;

public class Readers {

  public static Reader create(InputStream inStream,SerializationStrategy strategy) throws IOException {
    return strategy.createReader(inStream);
  }

}
