package typsy.api.serialization;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import typsy.api.Cells;

/**
 * strategy used to read and write streams of {@link Cells}
 * @author Mark Lester
 *
 */
public interface SerializationStrategy {
  public static SerializationStrategy JSON = new JsonStrategy();
//  public static WriteStrategy BSON = new WriteStrategy(){};
//  public static WriteStrategy XML = new WriteStrategy(){};
//  public static WriteStrategy KRYO = new WriteStrategy(){};
//  public static WriteStrategy SEQUENCE = new WriteStrategy(){};
//  public static WriteStrategy PARQUET = new WriteStrategy(){};
//  public static WriteStrategy AVRO = new WriteStrategy(){};
  Writer createWriter(OutputStream outStream) throws IOException;
  
  Reader createReader(InputStream inStream) throws IOException;
}