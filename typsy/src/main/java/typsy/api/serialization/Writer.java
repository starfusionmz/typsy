package typsy.api.serialization;

import java.io.Closeable;
import java.io.IOException;

import typsy.api.Cell;

public interface Writer extends Closeable {
  public enum Field{
    Visibility,Label,Namespace,Type,Value;
  }
  void write(Cell cell) throws IOException;
}
