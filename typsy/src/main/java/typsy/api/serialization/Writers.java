package typsy.api.serialization;

import java.io.IOException;
import java.io.OutputStream;

public class Writers {
  private Writers() {}

  public static Writer create(OutputStream outputStream, SerializationStrategy stategy) throws IOException {
    return stategy.createWriter(outputStream);
  }
}