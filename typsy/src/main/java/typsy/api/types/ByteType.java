package typsy.api.types;

import typsy.api.BaseValue;
import typsy.api.Value;

/**
 * byte type
 * 
 * @author Mark Lester
 *
 */
public class ByteType implements Type {

  public String getName() {
    return Byte.class.getName();
  }

  @Override
  public Value parse(Object value) {
    if(value instanceof Byte){
      return new BaseValue(this,value);      
    }
    throw new IllegalArgumentException("could not parse into value");
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T getValueAs(Class<T> type, Object value) {
    if(String.class.isAssignableFrom(type)){
      return (T) value.toString();
    }
    throw new IllegalArgumentException("Could not return value as type ");
  }
}
