package typsy.api.types;

import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import typsy.api.BaseValue;
import typsy.api.Value;

/**
 * naive first run at retrieving string values
 * 
 * @author Mark Lester
 *
 */
public class ObjectType implements Type {
  ObjectMapper mapper = new ObjectMapper();
  public String getName() {
    return String.class.getSimpleName();
  }

  @Override
  public Value parse(Object value) {
    @SuppressWarnings("unchecked")
    Map<String,Object> propMap = mapper.convertValue(value, Map.class);
    return new BaseValue(this, propMap);
  }

  @Override
  public <T> T getValueAs(Class<T> type, Object value) {
    return mapper.convertValue(value, type);
  }
}
