package typsy.api.types;

import typsy.api.BaseValue;
import typsy.api.Value;

/**
 * naive first run at retrieving string values
 * 
 * @author Mark Lester
 *
 */
public class StringType implements Type {

  public String getName() {
    return "string";
  }

  @Override
  public Value parse(Object value) {
    return new BaseValue(this,value.toString());
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T getValueAs(Class<T> type, Object value) {
    if(String.class.isAssignableFrom(type)){
      return (T) value.toString();
    }
    throw new IllegalArgumentException("Could not return value as type ");
  }
}
