package typsy.api.types;

import typsy.api.Cell;
import typsy.api.Value;

/**
 * interface used to determine how to use value portion of a {@link Cell}
 * 
 * @author Mark Lester
 *
 */
public interface Type {
  /**
   * name of the type
   * 
   * @return
   */
  String getName();

  /**
   * given an java object convert it to a value
   * 
   * @param object
   * @return
   */
  Value parse(Object object);

  /**
   * turn the given value into a the given type based of of what this is
   * 
   * @param type
   * @param value
   * @return
   */
  <T> T getValueAs(Class<T> type, Object value);
}
