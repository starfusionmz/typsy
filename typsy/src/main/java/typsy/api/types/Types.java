package typsy.api.types;

import java.util.Map;
import java.util.Optional;

import com.google.common.collect.ImmutableMap;

public class Types {
    static Map<String, Type> types = new ImmutableMap.Builder<String,Type>()
        .put("string",new StringType()).build();
	private Types(){}

	public static Optional<Type> find(String typeName){
		return Optional.ofNullable(types.get(typeName));
	}
}

