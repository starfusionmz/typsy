package typsy.api.visibility;

public class Visibilities {
  private Visibilities() {}

  public static Visibility create(String vis) {
    return new StringVis(vis);
  }
}


class StringVis implements Visibility {
  private final String vis;

  public StringVis(String vis) {
    this.vis = vis;
  }

  public String getVis() {
    return vis;
  }

  @Override
  public String toString() {
    return vis;
  }
}
