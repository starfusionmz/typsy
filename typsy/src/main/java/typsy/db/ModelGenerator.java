package typsy.db;

import org.h2.Driver;
import org.h2.jdbcx.JdbcDataSource;
import org.jooq.util.GenerationTool;
import org.jooq.util.h2.H2Database;
import org.jooq.util.jaxb.Configuration;
import org.jooq.util.jaxb.Database;
import org.jooq.util.jaxb.Generator;
import org.jooq.util.jaxb.Jdbc;
import org.jooq.util.jaxb.Target;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

public class ModelGenerator {

	public static void main(String args[]) throws Exception {
		String h2url = "jdbc:h2:~/test";
		String user = "sa";
		JdbcDataSource datasource = new JdbcDataSource();
		datasource.setURL(h2url);
		datasource.setUser(user);
		liquibase.database.Database database = DatabaseFactory.getInstance()
				.findCorrectDatabaseImplementation(new JdbcConnection(datasource.getConnection()));
		Liquibase liquibase = new Liquibase("db/changelog/db.changelog-master.xml",
				new ClassLoaderResourceAccessor(ModelGenerator.class.getClassLoader()), database);
		liquibase.update(new Contexts());

		Configuration configuration = new Configuration()
				.withJdbc(
						new Jdbc()
								.withDriver(
										Driver.class.getName())
								.withUrl(h2url).withUser(user))
				.withGenerator(new Generator()
						.withDatabase(new Database().withName(H2Database.class.getName()).withIncludes(".*")
								.withExcludes("").withInputSchema("PUBLIC"))
						.withTarget(new Target().withPackageName("typsy.db.jooq.generated")
								.withDirectory("target/generated/jooq/")));

		GenerationTool.generate(configuration);
	}
}
