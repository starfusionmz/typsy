/**
 * This class is generated by jOOQ
 */
package typsy.db.jooq.generated.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;

import typsy.db.jooq.generated.Keys;
import typsy.db.jooq.generated.Public;
import typsy.db.jooq.generated.tables.records.LinksRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Links extends TableImpl<LinksRecord> {

	private static final long serialVersionUID = -594092271;

	/**
	 * The reference instance of <code>PUBLIC.LINKS</code>
	 */
	public static final Links LINKS = new Links();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<LinksRecord> getRecordType() {
		return LinksRecord.class;
	}

	/**
	 * The column <code>PUBLIC.LINKS.ID</code>.
	 */
	public final TableField<LinksRecord, Integer> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>PUBLIC.LINKS.LINKTYPE</code>.
	 */
	public final TableField<LinksRecord, String> LINKTYPE = createField("LINKTYPE", org.jooq.impl.SQLDataType.VARCHAR.length(100).nullable(false), this, "");

	/**
	 * The column <code>PUBLIC.LINKS.SOURCE</code>.
	 */
	public final TableField<LinksRecord, Integer> SOURCE = createField("SOURCE", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

	/**
	 * The column <code>PUBLIC.LINKS.DESTINATION</code>.
	 */
	public final TableField<LinksRecord, Integer> DESTINATION = createField("DESTINATION", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

	/**
	 * Create a <code>PUBLIC.LINKS</code> table reference
	 */
	public Links() {
		this("LINKS", null);
	}

	/**
	 * Create an aliased <code>PUBLIC.LINKS</code> table reference
	 */
	public Links(String alias) {
		this(alias, LINKS);
	}

	private Links(String alias, Table<LinksRecord> aliased) {
		this(alias, aliased, null);
	}

	private Links(String alias, Table<LinksRecord> aliased, Field<?>[] parameters) {
		super(alias, Public.PUBLIC, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<LinksRecord, Integer> getIdentity() {
		return Keys.IDENTITY_LINKS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<LinksRecord> getPrimaryKey() {
		return Keys.PK_LINKS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<LinksRecord>> getKeys() {
		return Arrays.<UniqueKey<LinksRecord>>asList(Keys.PK_LINKS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Links as(String alias) {
		return new Links(alias, this);
	}

	/**
	 * Rename this table
	 */
	public Links rename(String name) {
		return new Links(name, null);
	}
}
