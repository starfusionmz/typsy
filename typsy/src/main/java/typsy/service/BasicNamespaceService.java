package typsy.service;

import static typsy.db.jooq.generated.tables.Namespaces.NAMESPACES;

import java.util.Collection;

import org.jooq.DSLContext;
import org.jooq.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import typsy.db.jooq.generated.tables.records.NamespacesRecord;
import typsy.web.model.Namespace;;

@Service
public class BasicNamespaceService implements NamespaceService {
  private DSLContext dslCtx;

  @Autowired
  public BasicNamespaceService(DSLContext dslCtx) {
    this.dslCtx = dslCtx;
  }

  @Override
  public void create(String name, String description) {
    Preconditions.checkNotNull(name);
    Preconditions.checkNotNull(description);
    dslCtx.insertInto(NAMESPACES).set(NAMESPACES.NAME, name)
        .set(NAMESPACES.DESCRIPTION, description).execute();
  }

  @Override
  public Namespace findByName(String name) {
    NamespacesRecord namespace = dslCtx.selectFrom(NAMESPACES).where(NAMESPACES.NAME.eq(name)).fetchOne();
    if(namespace!=null){
      return namespace.into(Namespace.class);
    }
    return null;
  }

  @Override
  public NamespacesRecord getNameSpace(Select<NamespacesRecord> query) {
    return query.fetchOne();
  }

  @Override
  public NamespacesRecord findById(int id) {
    return dslCtx.selectFrom(NAMESPACES).where(NAMESPACES.ID.eq(id)).fetchOne();
  }

  @Override
  public Collection<Namespace> getNamespaces() {
    return dslCtx.selectFrom(NAMESPACES).fetch().into(Namespace.class);
  }

  @Override
  public int removeAll() {
    return dslCtx.deleteFrom(NAMESPACES).execute();
  }
}
