package typsy.service;

import com.google.common.base.Preconditions;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.SelectConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import typsy.api.label.Label;
import typsy.api.label.Labels;
import typsy.db.jooq.generated.tables.Namespaces;
import typsy.db.jooq.generated.tables.records.LabelsRecord;
import typsy.db.jooq.generated.tables.records.NamespacesRecord;
import typsy.web.model.LabelModel;
import typsy.web.model.Namespace;

import java.util.Collection;
import java.util.List;

import static typsy.db.jooq.generated.tables.Labels.LABELS;

@Service
public class LabelService {
  private DSLContext dslCtx;
  private NamespaceService namespaceService;

  @Autowired
  public LabelService(DSLContext dslCtx, NamespaceService namespaceService) {
    this.dslCtx = dslCtx;
    this.namespaceService = namespaceService;
  }

  public void create(String name, String description, String namespaceName) {
    Label record = findLabel(name, namespaceName);
    if (record != null) {
      String msg =
          String.format("Label with name %s in namespace %s already exists", name, namespaceName);
      throw new DataIntegrityViolationException(msg);
    }
    Namespace namespaceRecord = namespaceService.findByName(namespaceName);
    Preconditions.checkNotNull(namespaceRecord, "Namespace must exist before creating a label");
    dslCtx.insertInto(LABELS).set(LABELS.NAME, name).set(LABELS.DESCRIPTION, description)
        .set(LABELS.NAMESPACE, namespaceRecord.getId()).returning(LABELS.ID).fetch()
        .getValue(0, LABELS.ID);
  }

  /**
   * find a the label with the given name and in the given namespace
   * 
   * @param name
   * @param namespace
   * @return
   */
  public Label findLabel(String name, String namespace) {
    return createLabel(findLabelRecord(name, namespace));
  }
  
  public LabelModel findById(Integer id) {
    LabelsRecord record = dslCtx.selectFrom(LABELS).where(LABELS.ID.eq(id)).fetchAny();
    NamespacesRecord namespace = namespaceService.findById(record.getNamespace());
    return new LabelModel(record,namespace);
  }
  /**
   * find a the label with the given name and in the given namespace
   * 
   * @param name
   * @param namespace
   * @return
   */
  public LabelsRecord findLabelRecord(String name, String namespace) {
    SelectConditionStep<Record> query = dslCtx.select().from(LABELS).join(Namespaces.NAMESPACES)
        .on(LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID))
        .where(LABELS.NAME.eq(name).and(Namespaces.NAMESPACES.NAME.eq(namespace)));
    return query.fetchAnyInto(LabelsRecord.class);
  }
  
  public LabelsRecord findLabelRecord(Label label) {
    SelectConditionStep<Record> query = dslCtx.select().from(LABELS).join(Namespaces.NAMESPACES)
        .on(LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID))
        .where(LABELS.NAME.eq(label.getName()).and(Namespaces.NAMESPACES.NAME.eq(label.getNamespace())));
    return query.fetchAnyInto(LabelsRecord.class);
  }

  Label createLabel(LabelsRecord labelRecord) {
    if (labelRecord == null)
      return null;
    NamespacesRecord namespaceRecord = namespaceService.findById(labelRecord.getNamespace());
    return Labels.create(labelRecord.getName(), namespaceRecord.getName(), labelRecord.getDescription());
  }

  /**
   * find all labels with the given name
   * 
   * @param name
   * @return
   */
  public List<LabelsRecord> findByName(String name) {
    SelectConditionStep<LabelsRecord> query = dslCtx.selectFrom(LABELS).where(LABELS.NAME.eq(name));
    return query.fetch();
  }
  
  /**
   * find all labels in the given namespace
   * 
   * @param namespace
   * @return
   */
  public List<LabelModel> findByNamespace(String namespace) {
    return dslCtx.select().from(LABELS).join(Namespaces.NAMESPACES)
        .on(Namespaces.NAMESPACES.ID.eq(LABELS.NAMESPACE))
        .where(Namespaces.NAMESPACES.NAME.eq(namespace))
        .fetch(new RecordMapper<Record, LabelModel>() {
      @Override
      public LabelModel map(Record record) {
        int id = record.getValue(LABELS.ID,int.class);
        String name = record.getValue(LABELS.NAME,String.class);
        String namespace = record.getValue(Namespaces.NAMESPACES.NAME,String.class);
        String description = record.getValue(LABELS.DESCRIPTION,String.class);
        LabelModel model = new LabelModel(name,namespace,description);
        model.setId(id);
        return model;
      }});
  }

  public Collection<LabelModel> findAll() {
    return dslCtx.select().from(LABELS).join(Namespaces.NAMESPACES).on(Namespaces.NAMESPACES.ID.eq(LABELS.NAMESPACE)).fetch(new RecordMapper<Record,LabelModel>() {

      @Override
      public LabelModel map(Record record) {
        int id = record.getValue(LABELS.ID,int.class);
        String name = record.getValue(LABELS.NAME,String.class);
        String namespace = record.getValue(Namespaces.NAMESPACES.NAME,String.class);
        String description = record.getValue(LABELS.DESCRIPTION,String.class);
        LabelModel model = new LabelModel(name,namespace,description);
        model.setId(id);
        return model;
      }});
  }

  public int removeAll() {
    return dslCtx.deleteFrom(LABELS).execute();
  }
}
