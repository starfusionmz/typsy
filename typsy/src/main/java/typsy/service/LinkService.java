package typsy.service;

import static typsy.db.jooq.generated.tables.Links.LINKS;

import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;

import typsy.api.label.Label;
import typsy.api.link.BasicLink;
import typsy.api.link.Link;
import typsy.api.link.LinkType;
import typsy.db.jooq.generated.tables.Labels;
import typsy.db.jooq.generated.tables.Links;
import typsy.db.jooq.generated.tables.Namespaces;
import typsy.db.jooq.generated.tables.records.LabelsRecord;
import typsy.db.jooq.generated.tables.records.LinksRecord;
import typsy.web.model.LabelModel;

/**
 * provides ways to query for links between labels. Links look like: label is name+namespace
 * srclabel dstlabel linktype
 * 
 * @author Mark Lester
 *
 */
public class LinkService {
  private DSLContext dslCtx;
  protected LabelService labelService;

  public LinkService(DSLContext dslCtx,LabelService labelService) {
    this.dslCtx = dslCtx;
    this.labelService = labelService;
  }

  public void create(LabelsRecord start, LabelsRecord end, LinkType type) {
    create(start.getId(), end.getId(), type);
  }
  
  public void create(int srcId, int dstId, LinkType type) {
    dslCtx.insertInto(Links.LINKS).set(LINKS.SOURCE, srcId)
        .set(LINKS.DESTINATION, dstId).set(LINKS.LINKTYPE, type.toString()).execute();
  }

  public LinksRecord findById(int id) {
    return dslCtx.selectFrom(LINKS).where(LINKS.ID.eq(id)).fetchOne().into(LinksRecord.class);
  }

  public List<LinksRecord> findBySourceNamespace(String namespace) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
        .join(Namespaces.NAMESPACES).on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID))
        .where(Namespaces.NAMESPACES.NAME.eq(namespace)).fetch().into(LinksRecord.class);
  }

  public List<LinksRecord> findByDestinationNamespace(String namespace) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS)
        .on(LINKS.DESTINATION.eq(Labels.LABELS.ID)).join(Namespaces.NAMESPACES)
        .on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID))
        .where(Namespaces.NAMESPACES.NAME.eq(namespace)).fetch().into(LinksRecord.class);
  }

  public List<LinksRecord> findByLinkTypeAndNamespace(LinkType type, String namespace) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
        .join(Namespaces.NAMESPACES).on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID))
        .where(Namespaces.NAMESPACES.NAME.eq(namespace).and(LINKS.LINKTYPE.eq(type.toString())))
        .fetch().into(LinksRecord.class);
  }

  public List<LinksRecord> findBySourceLabelName(String labelName) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
        .where(Labels.LABELS.NAME.eq(labelName)).fetch().into(LinksRecord.class);
  }

  public List<LinksRecord> findBySourceLabelNameAndLinkType(String labelName, LinkType linkType) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
        .where(Labels.LABELS.NAME.eq(labelName).and(LINKS.LINKTYPE.eq(linkType.toString()))).fetch()
        .into(LinksRecord.class);
  }

  public List<Link> findBySourceLabelAndLinkType(Label label, LinkType linkType) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
        .join(Namespaces.NAMESPACES).on(Labels.LABELS.NAMESPACE.eq(Namespaces.NAMESPACES.ID))
        .where(Labels.LABELS.NAME.eq(label.getName())
            .and(Namespaces.NAMESPACES.NAME.eq(label.getNamespace()))
            .and(LINKS.LINKTYPE.eq(linkType.toString())))
        .fetch(new RecordMapper<Record,Link>() {

          @Override
          public Link map(Record record) {
            LabelModel dest = labelService.findById(record.getValue(Links.LINKS.DESTINATION));
            return new BasicLink(label, dest, linkType);
          }
          
        });
  }

  public List<LinksRecord> findBySourceLabel(Label label) {
    return dslCtx.select().from(LINKS).join(Labels.LABELS).on(LINKS.SOURCE.eq(Labels.LABELS.ID))
        .where(Labels.LABELS.NAME.eq(label.getName()).and(label.getNamespace())).fetch()
        .into(LinksRecord.class);
  }
}
