package typsy.service;

import java.util.Collection;

import org.jooq.Select;

import typsy.db.jooq.generated.tables.records.NamespacesRecord;
import typsy.web.model.Namespace;

public interface NamespaceService {

  void create(String name, String description);

  Namespace findByName(String name);

  NamespacesRecord findById(int id);

  NamespacesRecord getNameSpace(Select<NamespacesRecord> query);

  Collection<Namespace> getNamespaces();
  
  /**
   * records removed
   * @return
   */
  int removeAll();

}
