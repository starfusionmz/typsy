package typsy.web;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import typsy.service.LabelService;
import typsy.web.model.LabelModel;

@Controller
@RequestMapping("/api/v1")
public class LabelController {
  private LabelService labelService;

  @Autowired
  public LabelController(LabelService labelService) {
    this.labelService = labelService;
  }

  @RequestMapping(path = "/labels", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<String> addLabel(@RequestBody @Valid LabelModel label) {
    labelService.create(label.getName(), label.getDescription(), label.getNamespace());
    return new ResponseEntity<String>(HttpStatus.CREATED);
  }

  @RequestMapping(path = "/label/{labelId}", method = RequestMethod.GET)
  @ResponseBody
  LabelModel getLabel(@PathVariable("labelId") int labelId) {
    return labelService.findById(labelId);
  }

  @RequestMapping(path = "/labels", method = RequestMethod.GET)
  @ResponseBody
  Collection<LabelModel> getLabels(@RequestParam(required=false) String namespace) {
    if(namespace!=null){
      return labelService.findByNamespace(namespace);
    }
    return labelService.findAll();
  }

  @ResponseStatus(value = HttpStatus.CONFLICT, reason = "data integrity violation")
  @ExceptionHandler(DataIntegrityViolationException.class)
  public void conflict(Exception e) {}
  
  @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "namespace must exist before creating a label")
  @ExceptionHandler(NullPointerException.class)
  public void nullPointer(Exception e) {}
}
