package typsy.web;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import typsy.api.link.LinkType;
import typsy.db.jooq.generated.tables.records.LinksRecord;
import typsy.service.LinkService;
import typsy.web.model.Namespace;

@Controller
@RequestMapping("/api/v1")
public class LinkController {

  private LinkService linkService;

  @Autowired
  public LinkController(LinkService linkService) {
    this.linkService = linkService;
  }

  @RequestMapping(path = "/links", method = RequestMethod.GET)
  public @ResponseBody Collection<Namespace> getLinks() {
    return Collections.emptyList();
  }

  @RequestMapping(path = "/links", method = RequestMethod.POST)
  public @ResponseBody void addLink(@RequestParam int srcLabel, @RequestParam int destLabel,
      LinkType linkType) {
    linkService.create(srcLabel, destLabel, linkType);
  }

  @RequestMapping(path = "/linktypes", method = RequestMethod.GET)
  public @ResponseBody Collection<LinkType> getLinkTypes() {
    return Arrays.asList(LinkType.values());
  }

  @RequestMapping(path = "/link/{linkId}", method = RequestMethod.GET)
  public @ResponseBody LinksRecord getLink(@PathVariable("linkId") int linkId) {
    return linkService.findById(linkId);
  }
}
