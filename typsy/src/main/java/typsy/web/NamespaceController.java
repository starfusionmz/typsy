package typsy.web;

import java.util.Collection;

import javax.validation.Valid;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import typsy.db.jooq.generated.tables.Labels;
import typsy.db.jooq.generated.tables.Links;
import typsy.db.jooq.generated.tables.Namespaces;
import typsy.service.NamespaceService;
import typsy.web.model.Namespace;

@Controller
@RequestMapping("/api/v1")
public class NamespaceController {

  private NamespaceService nameService;
  private DSLContext ctx;

  @Autowired
  public NamespaceController(NamespaceService nameService,DSLContext ctx) {
    this.nameService = nameService;
    this.ctx = ctx;
  }

  @RequestMapping(path = "/namespaces", method = RequestMethod.POST)
  public @ResponseBody ResponseEntity<String> addNamespace(@RequestBody @Valid Namespace namespace) {
    nameService.create(namespace.getName(), namespace.getDescription());
    return new ResponseEntity<String>(HttpStatus.CREATED);
  }
  
  @RequestMapping(path = "/namespaces", method = RequestMethod.GET)
  public @ResponseBody Collection<Namespace> getNamespaces() {
    return nameService.getNamespaces();
  }
  
  @RequestMapping(path = "/admin/clear", method = RequestMethod.GET)
  public @ResponseBody void deleteEverything() {
    ctx.truncate(Labels.LABELS).execute();
    ctx.truncate(Namespaces.NAMESPACES).execute();
    ctx.truncate(Links.LINKS).execute();
  }

  @RequestMapping(path = "/namespace/{namespace}", method = RequestMethod.GET)
  @ResponseBody
  Namespace getNamespace(@PathVariable("namespace") String namespaceName) {
    return nameService.findByName(namespaceName);
  }

  @ResponseStatus(value = HttpStatus.CONFLICT,reason="data integrity violation")
  @ExceptionHandler(DataIntegrityViolationException.class)
  public void conflict(Exception e) {
  }
}
