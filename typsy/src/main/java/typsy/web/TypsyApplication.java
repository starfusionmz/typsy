package typsy.web;

import javax.sql.DataSource;

import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.ExecuteListenerProvider;
import org.jooq.SQLDialect;
import org.jooq.TransactionProvider;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import typsy.db.ExceptionTranslator;
import typsy.db.SpringTransactionProvider;
import typsy.service.BasicNamespaceService;
import typsy.service.LabelService;
import typsy.service.LinkService;
import typsy.service.NamespaceService;


@SpringBootApplication
public class TypsyApplication {

  @Bean
  public UndertowEmbeddedServletContainerFactory embeddedServletContainerFactory() {
    UndertowEmbeddedServletContainerFactory factory = new UndertowEmbeddedServletContainerFactory();
    return factory;
  }

  @Bean
  public NamespaceService namespaceService(DSLContext dslContext) {
    return new BasicNamespaceService(dslContext);
  }

  @Bean
  public LabelService labelService(DSLContext dslContext, NamespaceService namespaceService) {
    return new LabelService(dslContext, namespaceService);
  }

  @Bean
  public LinkService linkService(DSLContext dslContext, LabelService labelService) {
    return new LinkService(dslContext, labelService);
  }

  @Bean
  public ExceptionTranslator exceptionTranslator() {
    return new ExceptionTranslator();
  }

  @Bean
  public ExecuteListenerProvider executeListenerProvider(ExceptionTranslator exceptionTranslator) {
    return new DefaultExecuteListenerProvider(exceptionTranslator);
  }

  @Bean
  public ConnectionProvider connectionProvider(DataSource dataSource) {
    return new DataSourceConnectionProvider(new TransactionAwareDataSourceProxy(dataSource));
  }

  @Bean
  public TransactionProvider transactionProvider() {
    return new SpringTransactionProvider();
  }


  @Bean
  public org.jooq.Configuration jooqConfig(ConnectionProvider connectionProvider,
      TransactionProvider transactionProvider, ExecuteListenerProvider executeListenerProvider) {

    return new DefaultConfiguration() //
        .derive(connectionProvider) //
        .derive(transactionProvider) //
        .derive(executeListenerProvider) //
        .derive(SQLDialect.H2);
  }

  @Bean
  public DSLContext dslContext(org.jooq.Configuration config) {
    return new DefaultDSLContext(config);
  }

  public static void main(String[] args) throws Exception {
    SpringApplication.run(TypsyApplication.class, args);
  }
}
