package typsy.web.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import typsy.api.label.Label;
import typsy.db.jooq.generated.tables.records.LabelsRecord;
import typsy.db.jooq.generated.tables.records.NamespacesRecord;

public class LabelModel implements Label{
  private int id;
  @NotNull
  @Size(min=2,max=100)
  @Pattern(regexp="[a-zA-Z]+")
  private String name; 
  @NotNull
  private String namespace;
  @NotNull
  private String description;
  
  public LabelModel(){}
  
  public LabelModel(String name, String namespace, String description) {
    this.name = name;
    this.namespace = namespace;
    this.description = description;
  }
  
  public LabelModel(LabelsRecord labelrecord,NamespacesRecord namespaceRecord) {
    this.id=labelrecord.getId();
    this.name = labelrecord.getName();
    this.namespace = namespaceRecord.getName();
    this.description = labelrecord.getDescription();    
  }
  
  @Override
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  @Override
  public String getNamespace() {
    return namespace;
  }
  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
  
  @Override
  public String toString() {
    return "LabelModel [id=" + id + ", name=" + name + ", namespace=" + namespace + ", description="
        + description + "]";
  }
}
