package typsy.web.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Namespace {
  @NotNull
  @Size(min = 3, max = 100)
  @Pattern(regexp="[a-zA-Z][0-9a-zA-Z_-]+")
  private String name;
  
  @NotNull
  @Size(min = 10, max = 100)
  private String description;
  
  private int id;

  public Namespace() {}

  public Namespace(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
