package typsy.api;

import java.util.Map;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectTypeTest {

  @Test
  public void testGeo() {
    Geo geo = new Geo(1.34, 2.45);
    ObjectMapper mapper = new ObjectMapper();
    @SuppressWarnings("unchecked")
    Map<String, Object> map = mapper.convertValue(geo, Map.class);
    Assert.assertEquals(geo.getLon(), map.get("lon"));
    Assert.assertEquals(geo, mapper.convertValue(map, Geo.class));
  }


  @Test
  public void testNode() {
    Node left1 = new Node("left1");
    Node right1 = new Node("right");
    Node root = new Node("root", left1, right1);
    ObjectMapper mapper = new ObjectMapper();
    @SuppressWarnings("unchecked")
    Map<String, Object> map = mapper.convertValue(root, Map.class);
    Assert.assertEquals(root.getLeft(), mapper.convertValue(map.get("left"),Node.class));
    Assert.assertEquals(root, mapper.convertValue(map, Node.class));
  }

}


class Node {

  private String name = "";
  private Node left;
  private Node right;

  public Node() {}

  public Node(String name) {
    this.setName(name);
  }

  public Node(String name, Node left, Node right) {
    this.right = right;
    this.left = left;
    this.setName(name);
  }

  public Node getLeft() {
    return left;
  }

  public void setLeft(Node left) {
    this.left = left;
  }

  public Node getRight() {
    return right;
  }

  public void setRight(Node right) {
    this.right = right;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Node [name=" + getName() + ", left=" + left + ", right=" + right + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, left, right);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj instanceof Node) {
      Node other = (Node) obj;
      return Objects.equals(name, other.name) && Objects.equals(left, other.left)
          && Objects.equals(right, other.right);
    }
    return false;
  }
}


class Geo {


  private double lat;
  private double lon;

  public Geo() {}

  Geo(double lat, double lon) {
    this.setLat(lat);
    this.setLon(lon);
  }

  public double getLon() {
    return lon;
  }

  public void setLon(double lon) {
    this.lon = lon;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(lat);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(lon);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Geo other = (Geo) obj;
    if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat))
      return false;
    if (Double.doubleToLongBits(lon) != Double.doubleToLongBits(other.lon))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Geo [lat=" + lat + ", lon=" + lon + "]";
  }
}
