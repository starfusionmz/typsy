package typsy.api.filter;

import com.google.common.collect.Sets;
import org.junit.Test;
import typsy.api.Cell;
import typsy.api.Cells;
import typsy.api.filter.LabelMatcher.MatchMode;
import typsy.api.label.Label;
import typsy.api.label.Labels;
import typsy.api.types.Type;
import typsy.api.types.Types;
import typsy.api.visibility.Visibilities;
import typsy.api.visibility.Visibility;

import static org.junit.Assert.assertTrue;

public class CellFilterTest {
  Label apple_fruit = Labels.create("apple", "fruit", "An apple is a fruit.");
  Label tomato_fruit = Labels.create("tomato", "fruit", "A tomato is a fruit, according to the PC master race.");
  Label tomato_vegetable = Labels.create("tomato", "vegetable", "A tomato is a vegetable, according to idiots.");
  Label celery_vegetable = Labels.create("celery", "vegetable", "celery is a veggie.");
  Visibility defaultVis = Visibilities.create("XXXX");
  Type stringType = Types.find("string").get();    
  
  Cell tomatoCell = Cells.create(tomato_fruit,defaultVis,stringType.parse("Tom Ato"));
  
  @Test
  public void testAllLabelFilter() {
    LabelMatcher tomato_fruit_both = new LabelMatcher(tomato_fruit,MatchMode.BOTH);
    LabelMatcher match_tomato = new LabelMatcher(tomato_fruit,MatchMode.LABEL);
    LabelMatcher match_vegetable = new LabelMatcher(tomato_vegetable,MatchMode.NAMESPACE);
    LabelMatcher apple_fruit_both = new LabelMatcher(apple_fruit,MatchMode.BOTH);
    
    AllLabelFilter filter = new AllLabelFilter(Sets.newHashSet(tomato_fruit_both));
    assertTrue(filter.apply(tomatoCell));
    
    filter = new AllLabelFilter(Sets.newHashSet(tomato_fruit_both,match_tomato));
    assertTrue(filter.apply(tomatoCell));
    
    filter = new AllLabelFilter(Sets.newHashSet(apple_fruit_both));
    assertTrue(!filter.apply(tomatoCell));
    
    filter = new AllLabelFilter(Sets.newHashSet(tomato_fruit_both,match_vegetable));
    assertTrue(!filter.apply(tomatoCell));
  }

  @Test
  public void testAnyLabelFilter() {
    LabelMatcher tomato_fruit_both = new LabelMatcher(tomato_fruit,MatchMode.BOTH);
    LabelMatcher match_tomato = new LabelMatcher(tomato_fruit,MatchMode.LABEL);
    LabelMatcher match_vegetable = new LabelMatcher(tomato_vegetable,MatchMode.NAMESPACE);
    LabelMatcher apple_fruit_both = new LabelMatcher(apple_fruit,MatchMode.BOTH);
    
    AnyLabelFilter filter = new AnyLabelFilter(Sets.newHashSet(tomato_fruit_both));
    assertTrue(filter.apply(tomatoCell));
    
    filter = new AnyLabelFilter(Sets.newHashSet(tomato_fruit_both,match_tomato));
    assertTrue(filter.apply(tomatoCell));
    
    filter = new AnyLabelFilter(Sets.newHashSet(apple_fruit_both));
    assertTrue(!filter.apply(tomatoCell));
    
    filter = new AnyLabelFilter(Sets.newHashSet(tomato_fruit_both,match_vegetable));
    assertTrue(filter.apply(tomatoCell));
  }
}
