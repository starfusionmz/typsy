package typsy.api.filter;

import org.junit.Test;
import typsy.api.filter.LabelMatcher.MatchMode;
import typsy.api.label.Label;
import typsy.api.label.Labels;

import static org.junit.Assert.assertTrue;

public class LabelMatcherTest {
  Label apple_fruit = Labels.create("apple", "fruit", "An apple is a fruit.");
  Label tomato_fruit = Labels.create("tomato", "fruit", "A tomato is a fruit, according to the PC master race.");
  Label tomato_vegetable = Labels.create("tomato", "vegetable", "A tomato is a vegetable, according to idiots.");
  Label celery_vegetable = Labels.create("celery", "vegetable", "celery is a veggie.");

  @Test
  public void testMatchOnBoth() {
    LabelMatcher matcher = new LabelMatcher(tomato_fruit, MatchMode.BOTH);
    assertTrue(matcher.apply(tomato_fruit));
    assertTrue(!matcher.apply(tomato_vegetable));
    assertTrue(!matcher.apply(apple_fruit));
  }

  @Test
  public void testMatchOnName() {
    LabelMatcher matcher = new LabelMatcher(tomato_fruit, MatchMode.LABEL);
    assertTrue(matcher.apply(tomato_fruit));
    assertTrue(matcher.apply(tomato_vegetable));
    assertTrue(!matcher.apply(apple_fruit));
  }

  @Test
  public void testMatchOnNamespace() {
    LabelMatcher matcher = new LabelMatcher(tomato_fruit, MatchMode.NAMESPACE);
    assertTrue(matcher.apply(tomato_fruit));
    assertTrue(!matcher.apply(tomato_vegetable));
    assertTrue(!matcher.apply(celery_vegetable));
    assertTrue(matcher.apply(apple_fruit));
  }
}
