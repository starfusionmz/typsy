package typsy.api.filter.fluent;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.FluentIterable;

import typsy.api.Cell;
import typsy.api.Cells;
import typsy.api.TypsyRegistry;
import typsy.api.TypsyService;
import typsy.api.Value;
import typsy.api.filter.CellFilter;
import typsy.api.label.Label;
import typsy.api.label.Labels;
import typsy.api.link.LinkType;
import typsy.api.serialization.Reader;
import typsy.api.types.Types;
import typsy.api.visibility.Visibilities;
import typsy.api.visibility.Visibility;
import typsy.service.LabelService;
import typsy.web.TypsyApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TypsyApplication.class)
public class LinkFilterTest {
  @Autowired
  private DataSource dataSource;
  String fruitNS = "fruit";
  String cookingNS = "cooking";
  String gardenNS = "garden";

  Label apple = Labels.create("apple", fruitNS);
  Label orange = Labels.create("orange", fruitNS);
  Label tomato_fruit = Labels.create("tomato", fruitNS);
  Label tomato_vegetable = Labels.create("tomato", cookingNS);
  Label tomato_garden = Labels.create("tomato", gardenNS);
  Label leaf = Labels.create("leaf", gardenNS);

  private TypsyService service;

  @Before
  public void setup() {
    // populate type db
    service = TypsyRegistry.getService(dataSource);
    LabelService labelService = service.getLabelService();
    service.getNamespaceService().create(fruitNS, "things in botany");
    service.getNamespaceService().create(cookingNS, "things in cooking");
    service.getNamespaceService().create(gardenNS, "things in a garden");
    service.getLabelService().create(apple.getName(), "", apple.getNamespace());
    service.getLabelService().create(orange.getName(), "", orange.getNamespace());
    service.getLabelService().create(tomato_fruit.getName(), "", tomato_fruit.getNamespace());
    service.getLabelService().create(tomato_vegetable.getName(), "",
        tomato_vegetable.getNamespace());
    service.getLabelService().create(tomato_garden.getName(), "", tomato_garden.getNamespace());
    service.getLabelService().create(leaf.getName(), "", leaf.getNamespace());
    service.getLinkService().create(labelService.findLabelRecord(tomato_fruit),
        labelService.findLabelRecord(tomato_vegetable), LinkType.ISA);
    service.getLinkService().create(labelService.findLabelRecord(tomato_vegetable),
        labelService.findLabelRecord(tomato_garden), LinkType.ISA);
    service.getLinkService().create(labelService.findLabelRecord(tomato_garden),
        labelService.findLabelRecord(tomato_fruit), LinkType.ISA);
    service.getLinkService().create(labelService.findLabelRecord(tomato_garden),
        labelService.findLabelRecord(leaf), LinkType.HASA);
  }

  @After
  public void cleanup() {
    service.getContext().deleteFrom(typsy.db.jooq.generated.tables.Labels.LABELS).execute();
    service.getContext().deleteFrom(typsy.db.jooq.generated.tables.Namespaces.NAMESPACES).execute();
    service.getContext().deleteFrom(typsy.db.jooq.generated.tables.Links.LINKS).execute();
  }

  @Test
  public void testMatchEitherSideOfLink() throws IOException {

    // create data
    Value value = Types.find("string").get().parse("red");
    Visibility vis = Visibilities.create("XXXX");
    Cell cell1 = Cells.create(apple, vis, value);
    Cell cell2 = Cells.create(tomato_fruit, vis, value);
    Cell cell3 = Cells.create(tomato_vegetable, vis, value);

    Reader reader = Mockito.mock(Reader.class);
    List<Cell> stream = Arrays.asList(cell1, cell2, cell3);
    Mockito.when(reader.iterator()).thenReturn(stream.iterator());
    Mockito.when(reader.spliterator()).thenReturn(stream.spliterator());
    Mockito.when(reader.stream()).thenReturn(stream.stream());
    LinkFilterBuilder filterBuilder = service.createLabelFilter();
    CellFilter tomatofilter = filterBuilder.on(LinkDirection.EITHER)
        .where(
            filterBuilder.label().eq(tomato_fruit).and(filterBuilder.linktype().eq(LinkType.ISA)))
        .build();
    List<Cell> tomatoResults = reader.stream().filter(tomatofilter).collect(Collectors.toList());
    assertEquals(2, tomatoResults.size());
    System.out.println("results: " + tomatoResults);
  }

  @Test
  public void testMatchSourceOfLink() throws IOException {
    // create data
    Value value = Types.find("string").get().parse("red");
    Visibility vis = Visibilities.create("XXXX");
    Cell cell1 = Cells.create(apple, vis, value);
    Cell cell2 = Cells.create(tomato_fruit, vis, value);
    Cell cell3 = Cells.create(tomato_vegetable, vis, value);
    Cell cell4 = Cells.create(leaf, vis, value);

    Reader reader = Mockito.mock(Reader.class);
    Mockito.when(reader.iterator())
        .thenReturn(Arrays.asList(cell1, cell2, cell3, cell4).iterator());
    LinkFilterBuilder filterBuilder = service.createLabelFilter();
    CellFilter tomatofilter = filterBuilder.on(LinkDirection.SOURCE)
        .where(
            filterBuilder.label().eq(tomato_garden).and(filterBuilder.linktype().eq(LinkType.HASA)))
        .build();
    List<Cell> tomatoResults = FluentIterable.from(reader).filter(tomatofilter).toList();
    System.out.println("results: " + tomatoResults);
    assertEquals(1, tomatoResults.size());

  }

}
