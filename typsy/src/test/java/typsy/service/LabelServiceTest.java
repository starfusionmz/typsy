package typsy.service;

import java.util.List;

import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import typsy.api.label.Label;
import typsy.db.jooq.generated.tables.Labels;
import typsy.db.jooq.generated.tables.Namespaces;
import typsy.db.jooq.generated.tables.records.LabelsRecord;
import typsy.web.TypsyApplication;
import typsy.web.model.LabelModel;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TypsyApplication.class,webEnvironment = WebEnvironment.RANDOM_PORT)
public class LabelServiceTest {

  private static final String NAMESPACE = "namespace";

  @Autowired
  LabelService labelService;

  @Autowired
  NamespaceService namespaceService;
  
  @Autowired
  DSLContext dslContext;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void setup() {
    namespaceService.create(NAMESPACE, "test namespace");
  }

  @After
  public void cleanup() {
    dslContext.deleteFrom(Namespaces.NAMESPACES).execute();
    dslContext.deleteFrom(Labels.LABELS).execute();
  }

  @Test
  public void testCreateLabel() {
    String labelName = "test";
    String labelDescription = "test description";
    labelService.create(labelName, labelDescription, NAMESPACE);
    LabelsRecord label = labelService.findLabelRecord(labelName, NAMESPACE);
    Assert.assertNotNull(label);
    Assert.assertEquals(labelName, label.getName());
    Assert.assertEquals(labelDescription, label.getDescription());
    thrown.expect(DataIntegrityViolationException.class);
    labelService.create(labelName, labelDescription, NAMESPACE);
  }

  @Test
  public void testFindLabel() {
    labelService.create("test2", "test2 desc", NAMESPACE);

    Label labelFromDB = labelService.findLabel("test2", NAMESPACE);
    Assert.assertNotNull(labelFromDB);
    Assert.assertEquals("test2", labelFromDB.getName());
    Assert.assertEquals(NAMESPACE, labelFromDB.getNamespace());
  }

  @Test
  public void testFindByNamespace() {
    namespaceService.create("namespace2", "");
    labelService.create("test2", "test2 desc", "namespace2");
    labelService.create("test2", "test2 desc", NAMESPACE);

    List<LabelModel> labels = labelService.findByNamespace(NAMESPACE);
    Assert.assertEquals(1, labels.size());
    LabelModel label = labels.get(0);
    Assert.assertEquals("test2", label.getName());
    Assert.assertEquals("test2 desc", label.getDescription());
  }
  
  @Test
  public void testFindByName() {
    labelService.create("test1", "test1 desc", NAMESPACE);
    labelService.create("test2", "test2 desc", NAMESPACE);
    List<LabelsRecord> labels = labelService.findByName("test2");
    Assert.assertEquals(1, labels.size());
    LabelsRecord label = labels.get(0);
    Assert.assertEquals("test2", label.getName());
    Assert.assertEquals("test2 desc", label.getDescription());
  }
}
