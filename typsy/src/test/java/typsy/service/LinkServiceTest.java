package typsy.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import typsy.api.link.LinkType;
import typsy.db.jooq.generated.tables.Labels;
import typsy.db.jooq.generated.tables.Links;
import typsy.db.jooq.generated.tables.Namespaces;
import typsy.db.jooq.generated.tables.records.LabelsRecord;
import typsy.db.jooq.generated.tables.records.LinksRecord;
import typsy.web.TypsyApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TypsyApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class LinkServiceTest {

  @Autowired
  LabelService labelService;

  @Autowired
  NamespaceService namespaceService;

  @Autowired
  LinkService linkService;

  @Autowired
  DSLContext dslContext;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void setup() {}

  @After
  public void cleanup() {
    dslContext.deleteFrom(Namespaces.NAMESPACES).execute();
    dslContext.deleteFrom(Labels.LABELS).execute();
    dslContext.deleteFrom(Links.LINKS).execute();
  }

  @Test
  public void testCreateLink() {
    namespaceService.create("namespace", "a cool namespace");
    namespaceService.create("namespace2", "another cool namespace");
    labelService.create("label1", "label1 is a cool label", "namespace");
    labelService.create("label2", "label2 is also a cool label", "namespace2");
    LabelsRecord label1 = labelService.findLabelRecord("label1", "namespace");
    LabelsRecord label2 = labelService.findLabelRecord("label2", "namespace2");
    linkService.create(label1, label2, LinkType.ISA);
  }

  @Test
  public void testFindByNamespace() {
    namespaceService.create("namespace", "a cool namespace");
    namespaceService.create("namespace2", "another cool namespace");
    labelService.create("label1", "label1 is a cool label", "namespace");
    labelService.create("label2", "label2 is also a cool label", "namespace2");
    LabelsRecord label1 = labelService.findLabelRecord("label1", "namespace");
    LabelsRecord label2 = labelService.findLabelRecord("label2", "namespace2");
    linkService.create(label1, label2, LinkType.ISA);
    linkService.create(label2, label1, LinkType.SUPERCEDES);

    List<LinksRecord> links = linkService.findBySourceNamespace("namespace");
    assertEquals(1, links.size());
    LinksRecord link = links.get(0);
    assertEquals(label1.getId(), link.getSource());
    assertEquals(label2.getId(), link.getDestination());
    assertEquals(LinkType.ISA.toString(), link.getLinktype());
  }

  @Test
  public void testFindByNamespaceAndType() {
    namespaceService.create("namespace", "a cool namespace");
    namespaceService.create("namespace2", "another cool namespace");
    labelService.create("label1", "label1 is a cool label", "namespace");
    labelService.create("label2", "label2 is also a cool label", "namespace2");
    LabelsRecord label1 = labelService.findLabelRecord("label1", "namespace");
    LabelsRecord label2 = labelService.findLabelRecord("label2", "namespace2");
    linkService.create(label1, label2, LinkType.ISA);
    linkService.create(label1, label2, LinkType.SUPERCEDES);

    List<LinksRecord> links = linkService.findByLinkTypeAndNamespace(LinkType.ISA, "namespace");
    assertEquals(1, links.size());
    LinksRecord link = links.get(0);
    assertEquals(label1.getId(), link.getSource());
    assertEquals(label2.getId(), link.getDestination());
    assertEquals(LinkType.ISA.toString(), link.getLinktype());
  }

}
