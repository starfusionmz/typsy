package typsy.service;

import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import typsy.db.jooq.generated.tables.Namespaces;
import typsy.web.TypsyApplication;
import typsy.web.model.Namespace;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TypsyApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class NamespaceServiceTest {

  @Autowired
  NamespaceService namespaceService;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Autowired
  private DSLContext dslContext;

  @After
  public void cleanup() {
    dslContext.deleteFrom(Namespaces.NAMESPACES).execute();
  }

  @Test
  public void testCreateNameSpace() {
    namespaceService.create("test", "test");
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void testFailOnDuplicateCreate() {
    namespaceService.create("test", "test");
    namespaceService.create("test", "test");
  }

  @Test
  public void testFindNamespaceByName() {
    namespaceService.create("test", "test");
    Namespace namespace = namespaceService.findByName("test");
    Assert.assertNotNull(namespace);
    Assert.assertEquals("test", namespace.getName());
    Assert.assertEquals("test", namespace.getDescription());
  }
}
