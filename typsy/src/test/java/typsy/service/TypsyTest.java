package typsy.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Optional;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.FluentIterable;

import typsy.api.Cell;
import typsy.api.Cells;
import typsy.api.TypsyRegistry;
import typsy.api.TypsyService;
import typsy.api.Value;
import typsy.api.filter.CellFilter;
import typsy.api.filter.fluent.LinkDirection;
import typsy.api.filter.fluent.LinkFilterBuilder;
import typsy.api.label.Label;
import typsy.api.label.Labels;
import typsy.api.link.LinkType;
import typsy.api.serialization.Reader;
import typsy.api.serialization.Readers;
import typsy.api.serialization.SerializationStrategy;
import typsy.api.serialization.Writer;
import typsy.api.serialization.Writers;
import typsy.api.types.Type;
import typsy.api.types.Types;
import typsy.api.visibility.Visibilities;
import typsy.api.visibility.Visibility;
import typsy.web.TypsyApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TypsyApplication.class,webEnvironment = WebEnvironment.RANDOM_PORT)
public class TypsyTest {
  @Autowired
  private DataSource dataSource;

  // test types
  // ip
  // geopoint
  // file-structure,

  /**
   * This test is an example of how to store an ip. Will evolve over time as typing gets string in
   * Typsy
   * 
   * @throws IOException
   */
  @Test
  public void testCreateIp() throws IOException {
    Label ippros_label = Labels.create("ip", "networking.experts", "seriously legit ip, no joke.");
    Label myip_label = Labels.create("ip", "mark.awesome", "more legit than experts. use this.");
    
    //populate type db
    TypsyService service = TypsyRegistry.getService(dataSource);
    LabelService labelService = service.getLabelService();
    service.getNamespaceService().create(myip_label.getNamespace(), "");
    service.getNamespaceService().create(ippros_label.getNamespace(), "");
    
    service.getLabelService().create(myip_label.getName(), "", myip_label.getNamespace());
    service.getLabelService().create(ippros_label.getName(), "", ippros_label.getNamespace());
    
    service.getLinkService().create(labelService.findLabelRecord(myip_label), labelService.findLabelRecord(ippros_label), LinkType.ISA);
    //create data
    Optional<Type> type = Types.find("string");
    Value value = type.get().parse("192.168.1.1");
    Visibility vis = Visibilities.create("XXXX");
    Cell cell1 = Cells.create(myip_label, vis, value);
    Cell cell2 = Cells.create(ippros_label, vis, value);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    Writer writer = Writers.create(baos, SerializationStrategy.JSON);
    writer.write(cell1);
    writer.write(cell2);
    writer.close();
    byte[] bytes = baos.toByteArray();
    String result = new String(bytes, Charset.forName("utf8"));
    System.out.println("JSON: "+result);

    //read data
    ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
    Reader reader = Readers.create(bis, SerializationStrategy.JSON);
    LinkFilterBuilder filterBuilder = service.createLabelFilter();
    CellFilter labelFilter = filterBuilder.on(LinkDirection.EITHER)
        .where(filterBuilder.linktype().eq(LinkType.ISA).and(filterBuilder.label().eq(myip_label)))
        .build();
    System.out.println("results "+FluentIterable.from(reader).filter(labelFilter).toList());
  }

  /**
   * This test is an example of how to store a geo. geo is interesting because it composed of 2 or
   * more things and without the other the geo doesn't mean anything
   */
  @Test
  public void testCreateGeoPoint() {
    Assert.fail();
  }

  /**
   * This test is an example of how to store an file structure. This one is interesting because file
   * is essentially a graph with arbitrary nesting. * Does it make sense to store the nesting?, * On
   * retrieval does it make sense to preserve nesting
   * 
   * @throws IOException
   */
  @Test
  public void testCreateFileStructure() throws IOException {
    Assert.fail();
  }

  // give me anything in networkingpros namespace


  // give me all things networkpros labeled as ip
  // give me all things labeled with network pros ip
  @Test
  public void testConsumption() {
    Reader reader = null;// Readers.create("cells.json");
//    CellMatcher myLinks =
//        new CellMatcher().eqls(Field.Namespace, "mark.awesome").and().eqls(Field.Label, "ip");
//    CellMatcher official =
//        new CellMatcher().eqls(Field.Namespace, "networkpros").and().eqls(Field.Label, "ip");
//    for (Cell cell : reader) {
//      myLinks.collect(cell);
//      official.collect(cell);
//    }
  }

}
