package typsy.web.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import typsy.db.jooq.generated.tables.records.LabelsRecord;
import typsy.service.LabelService;
import typsy.service.NamespaceService;
import typsy.web.LabelController;
import typsy.web.model.LabelModel;
import typsy.web.model.Namespace;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LabelControllerTest {
  @Autowired
  LabelController labelController;

  @Autowired
  NamespaceService namespaceService;
  
  @Autowired
  LabelService labelService;

  private TestRestTemplate restTemplate = new TestRestTemplate();

  @Value("${local.server.port}")
  int port;

  ObjectMapper mapper = new ObjectMapper();

  private String baseUrl;

  private HttpHeaders requestHeaders;

  private Namespace namespace = new Namespace("mark", "so hot make dragons retire");


  @Before
  public void setup() {
    baseUrl = "http://localhost:" + port + "/api/v1/";
    requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.APPLICATION_JSON);
    labelService.removeAll();
    namespaceService.removeAll();
  }

  @Test
  public void testAddLabel() throws Exception {
    ResponseEntity<String> response = createNamespace(namespace);    
    Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assert.assertNotNull(namespaceService.findByName(namespace.getName()));
    LabelModel label = new LabelModel("ip", namespace.getName(), "an ip");
    response = createLabel(label);    
    Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assert.assertNotNull(labelService.findLabelRecord(label));
  }

  @Test
  public void testFailOnCreateTwice() throws Exception {
    ResponseEntity<String> response = createNamespace(namespace);    
    LabelModel label = new LabelModel("ip", namespace.getName(), "an ip");
    response = createLabel(label);
    response = createLabel(label);
    //TODO this needs to be bad request
    Assert.assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
  }
  
  @Test
  public void testFailOnNoNamespace() throws Exception {
    LabelModel label = new LabelModel("ip", namespace.getName(), "an ip");
    ResponseEntity<String> response = createLabel(label);
    Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
  }


  @Test
  public void testGetLabel() throws Exception {
    createNamespace(namespace);    
    LabelModel label = new LabelModel("ip", namespace.getName(), "an ip");
    createLabel(label);
    LabelsRecord storedLabel = labelService.findLabelRecord(label);

    String getNamespaceURL = baseUrl + "label/" + storedLabel.getId();
    System.out.println(getNamespaceURL);
    ResponseEntity<LabelModel> responseEntity =
        restTemplate.getForEntity(getNamespaceURL, LabelModel.class);
    Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    Assert.assertEquals(label.getName(), responseEntity.getBody().getName());
    Assert.assertNotNull(responseEntity.getBody().getId());
  }

  @Test
  public void testGetLabels() throws Exception {
    createNamespace(namespace);
    LabelModel label = new LabelModel("ip", namespace.getName(), "an ip");
    createLabel(label);
    createLabel(new LabelModel("orange", namespace.getName(), "a orange colored fruit"));
    String getNamespaceURL = baseUrl + "labels/";
    System.out.println(getNamespaceURL);
    //TODO this should probably not use one of the genrated classes
    ResponseEntity<LabelModel[]> responseEntity =
    restTemplate.getForEntity(getNamespaceURL, LabelModel[].class);
    Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    Assert.assertEquals(2, responseEntity.getBody().length);
  }

  ResponseEntity<String> createNamespace(Namespace namespace) throws Exception {
    String addNamespaceURL = baseUrl + "namespaces";

    HttpEntity<String> httpEntity =
        new HttpEntity<>(mapper.writeValueAsString(namespace), requestHeaders);

    ResponseEntity<String> responseEntity =
        restTemplate.postForEntity(addNamespaceURL, httpEntity, String.class);
    return responseEntity;
  }
  
  ResponseEntity<String> createLabel(LabelModel label) throws Exception {
    String addLabelURL = baseUrl + "labels";

    HttpEntity<String> httpEntity =
        new HttpEntity<>(mapper.writeValueAsString(label), requestHeaders);

    ResponseEntity<String> responseEntity =
        restTemplate.postForEntity(addLabelURL, httpEntity, String.class);
    return responseEntity;
  }
}
