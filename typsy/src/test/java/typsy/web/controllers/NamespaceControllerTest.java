package typsy.web.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import typsy.service.NamespaceService;
import typsy.web.NamespaceController;
import typsy.web.model.Namespace;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class NamespaceControllerTest {
  @Autowired
  NamespaceController namespaceController;

  @Autowired
  NamespaceService namespaceServices;

  private TestRestTemplate restTemplate = new TestRestTemplate();

  @Value("${local.server.port}")
  int port;

  ObjectMapper mapper = new ObjectMapper();

  private String baseUrl;

  private HttpHeaders requestHeaders;

  private Namespace namespace = new Namespace("mark", "so hot make dragons retire");

  @Before
  public void setup() {
    baseUrl = "http://localhost:" + port + "/api/v1/";
    requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.APPLICATION_JSON);
    namespaceServices.removeAll();
  }

  @Test
  public void testAddNameSpace() throws Exception {
    ResponseEntity<String> response = createNamespace(namespace);
    Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assert.assertNotNull(namespaceServices.findByName(namespace.getName()));
  }

  @Test
  public void testFailOnCreateTwice() throws Exception {
    ResponseEntity<String> response = createNamespace(namespace);
    response = createNamespace(namespace);
    Assert.assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
  }

  @Test
  public void testGetNameSpace() throws Exception {
    createNamespace(namespace);

    String getNamespaceURL = baseUrl + "namespace/" + namespace.getName();
    System.out.println(getNamespaceURL);
    ResponseEntity<Namespace> responseEntity =
        restTemplate.getForEntity(getNamespaceURL, Namespace.class);
    Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    Assert.assertEquals(namespace.getName(), responseEntity.getBody().getName());
    Assert.assertNotNull(responseEntity.getBody().getId());
  }

  @Test
  public void testGetNameSpaces() throws Exception {
    createNamespace(namespace);
    createNamespace(new Namespace("networkpros", "not as cool as mark.awesome"));

    String getNamespaceURL = baseUrl + "namespaces/";
    System.out.println(getNamespaceURL);
    ResponseEntity<Namespace[]> responseEntity =
        restTemplate.getForEntity(getNamespaceURL, Namespace[].class);
    Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    Assert.assertEquals(2, responseEntity.getBody().length);
  }

  ResponseEntity<String> createNamespace(Namespace namespace) throws Exception {
    String addNamespaceURL = baseUrl + "namespaces";

    HttpEntity<String> httpEntity =
        new HttpEntity<>(mapper.writeValueAsString(namespace), requestHeaders);

    ResponseEntity<String> responseEntity =
        restTemplate.postForEntity(addNamespaceURL, httpEntity, String.class);
    return responseEntity;
  }
}
